# Git Summary
Git is a free and open source distributed version control system designed to handle everything from small to very large projects with speed and efficiency. Git is easy to learn and has a tiny footprint with lightning fast performance.

![Summary](https://wuxiaomin98.files.wordpress.com/2016/04/gitflow.png?w=600)
##Vocabulary
- **Repository** is the collection of files and folders for git to track.  
- **Commit** saves your work in the Repository.  
- **Push** means adding your commits.  
- **Branch** is the separate part of the main code.  
- **Merge** means integrating the branch with main code.  
- **Clone** means making a copy of main branch into your computer.  
- **Fork** means making a copy of main branch in your Repository with your username.  

![Vocabulary](https://saracope.github.io/git-the-basics/images/git_staging.png)
## Getting Started  
### How to Install Git  
**_For Linux_**: sudo apt-get install git  
**_For Windows_**: Download and run it    

## Git Internals

1. **Modified** means changed the file but not committed.
2. **Staged** means marked the changes but not committed yet.
3. **Committed** means that changes have been saved to repository.


## Git Workflow  
1. **Clone repo**:$ git clone (link to repository) 
2. **Create new branch**:
    *$ git checkout master
    *$ git checkout -b (your branch name)
3. **Staging changes**:$ git add .
4. **Commit changes**:$ git commit -sv
5. **Push Files**:$ git push origin (branch name)

![Workflow](https://res.cloudinary.com/practicaldev/image/fetch/s--M_fHUEqA--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://thepracticaldev.s3.amazonaws.com/i/128hsgntnsu9bww0y8sz.png)
